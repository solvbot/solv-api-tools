from django.utils.functional import cached_property
from rest_framework import serializers


class PrimaryKeyField(serializers.IntegerField):
    default_error_messages = {
        'required': 'To pole nie może być puste.',
        'null': 'To pole nie może być puste.',
        'invalid': 'Wybierz poprawną wartość.'
    }


class MissingDataErrorSerializer(serializers.Serializer):
    name = serializers.CharField(read_only=True)
    fill_url = serializers.URLField(read_only=True)
    fields = serializers.ListField(child=serializers.CharField(read_only=True), read_only=True)


class ErrorsField(serializers.ListField):
    def __init__(self, *args, **kwargs):
        kwargs['child'] = serializers.CharField(read_only=True)
        kwargs['read_only'] = True
        kwargs['required'] = False

        super().__init__(*args, **kwargs)


class ErrorsSerializer(serializers.Serializer):
    def __init__(self,  *args, **kwargs):
        kwargs['read_only'] = True

        self.potential_error_field_names = kwargs.pop('potential_error_fields', None)
        super().__init__(*args, **kwargs)

    @cached_property
    def fields(self):
        fields = super().fields

        for pef in self.potential_error_field_names:
            fields[pef] = ErrorsField()

        return fields

    non_field_errors = ErrorsField()


class TemplateErrorsSerializer(ErrorsSerializer):
    missing_data = serializers.ListField(child=MissingDataErrorSerializer(read_only=True), read_only=True)
