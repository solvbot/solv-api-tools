from rest_framework import serializers


class KrsField(serializers.CharField):
    def to_internal_value(self, data):
        value = super().to_internal_value(data)
        return value.lstrip("0")


class RegonField(serializers.CharField):
    def to_internal_value(self, data):
        value = super().to_internal_value(data)
        return value.replace(' ', '').replace('-', '')


class NipField(serializers.CharField):
    def to_internal_value(self, data):
        value = super().to_internal_value(data)
        return value.replace(' ', '').replace('-', '')